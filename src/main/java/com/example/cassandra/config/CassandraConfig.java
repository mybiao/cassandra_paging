package com.example.cassandra.config;

import com.datastax.oss.driver.api.core.CqlSession;
import com.datastax.oss.driver.api.core.DefaultConsistencyLevel;
import com.datastax.oss.driver.api.core.config.DefaultDriverOption;
import com.datastax.oss.driver.api.core.config.DriverConfigLoader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.InetSocketAddress;

@Configuration
public class CassandraConfig {

    @Bean
    public CqlSession cqlSession(){
        DriverConfigLoader driverConfigLoader = DriverConfigLoader.programmaticBuilder()
                .withString(DefaultDriverOption.PROTOCOL_COMPRESSION,"lz4")
                .withString(DefaultDriverOption.REQUEST_CONSISTENCY, DefaultConsistencyLevel.ONE.name())
                .withInt(DefaultDriverOption.REQUEST_PAGE_SIZE,2000)
                .build();
        InetSocketAddress address=new InetSocketAddress("localhost",9042);
        return CqlSession.builder()
                .addContactPoint(address)
                .withLocalDatacenter("datacenter1")
                .withConfigLoader(driverConfigLoader)
                .build();

    }
}
