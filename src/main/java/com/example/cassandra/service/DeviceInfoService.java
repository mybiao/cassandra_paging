package com.example.cassandra.service;

import com.datastax.oss.driver.api.core.CqlSession;
import com.datastax.oss.driver.api.core.MappedAsyncPagingIterable;
import com.datastax.oss.driver.api.core.PagingIterable;
import com.datastax.oss.driver.api.core.cql.AsyncResultSet;
import com.datastax.oss.driver.api.core.cql.ResultSet;
import com.datastax.oss.driver.api.core.cql.Row;
import com.datastax.oss.driver.api.core.cql.SimpleStatement;
import com.datastax.oss.driver.api.core.paging.OffsetPager;
import com.datastax.oss.driver.api.querybuilder.QueryBuilder;
import com.example.cassandra.dao.DeviceInfoDao;
import com.example.cassandra.entity.DeviceInfoEntity;
import com.example.cassandra.mapper.DeviceInfoMapper;
import com.example.cassandra.mapper.DeviceInfoMapperBuilder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicLong;

@Service
public class DeviceInfoService {

    private final CqlSession cqlSession;

    private final DeviceInfoDao deviceInfoDao;

    public DeviceInfoService(CqlSession cqlSession) {
        this.cqlSession = cqlSession;
        DeviceInfoMapper deviceInfoMapper=new DeviceInfoMapperBuilder(cqlSession).build();
        deviceInfoDao = deviceInfoMapper.deviceInfoDao("mydb","device_info");
    }

    public CompletionStage<Void> saveOneDeviceAsync(DeviceInfoEntity deviceInfo){
        return deviceInfoDao.saveOne(deviceInfo);
    }
    public DeviceInfoEntity saveOneDevice(DeviceInfoEntity deviceInfo){
        return deviceInfoDao.saveOneDevice(deviceInfo);
    }

    public CompletionStage<Integer> countDeviceInfo() {
        String sql = "select create_time from mydb.device_info where product_id=100 and device_id='dev00001'";
        SimpleStatement statement = SimpleStatement.newInstance(sql)
                .setPageSize(4000);
        CompletionStage<AsyncResultSet> resultSetFuture = cqlSession.executeAsync(statement);
        return resultSetFuture.thenCompose(rs->countRows(rs,0));
    }

    private CompletionStage<Integer> countRows(AsyncResultSet rs, int preCount){
        int count = preCount;
        for (Row ignored : rs.currentPage()){
            count++;
        }
        if (rs.hasMorePages()){
            int finalCount = count;
            return rs.fetchNextPage().thenCompose(rs1->countRows(rs1, finalCount));
        }
        return CompletableFuture.completedFuture(count);
    }

    public List<DeviceInfoEntity> selectPage(Integer page, Integer size){
        OffsetPager offsetPager=new OffsetPager(size);
        PagingIterable<DeviceInfoEntity> pagingIterable = deviceInfoDao.selectByProductIdAndDeviceId(100L,"dev00001");
        OffsetPager.Page<DeviceInfoEntity> page1 = offsetPager.getPage(pagingIterable,page);
        return page1.getElements();
    }

    public CompletionStage<OffsetPager.Page<DeviceInfoEntity>> selectPageAsync(Integer page,Integer size){
        OffsetPager offsetPager=new OffsetPager(size);
        CompletionStage<MappedAsyncPagingIterable<DeviceInfoEntity>> pagingIterable = deviceInfoDao.selectByProductIdAndDeviceIdAsync(100L,"dev00001");
        return pagingIterable.thenCompose(map-> offsetPager.getPage(map,page));
    }
}
