package com.example.cassandra.controller;

import com.datastax.oss.driver.api.core.paging.OffsetPager;
import com.example.cassandra.entity.DeviceInfoEntity;
import com.example.cassandra.service.DeviceInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;

@RestController
public class DeviceInfoController {

    private final DeviceInfoService deviceInfoService;

    private static final Logger log = LoggerFactory.getLogger(DeviceInfoController.class);

    public DeviceInfoController(DeviceInfoService deviceInfoService) {
        this.deviceInfoService = deviceInfoService;
    }

    @PutMapping("/device_info")
    public int insertOneAsync(Long productId,String deviceId){
        DeviceInfoEntity deviceInfo=new DeviceInfoEntity();
        deviceInfo.setProductId(productId);
        deviceInfo.setDeviceId(deviceId);
        deviceInfo.setCreateTime(Instant.ofEpochMilli(System.currentTimeMillis()));
        deviceInfo.setV1(UUID.randomUUID().toString());
        deviceInfo.setV2(UUID.randomUUID().toString());
        deviceInfo.setV3(UUID.randomUUID().toString());
        deviceInfoService.saveOneDeviceAsync(deviceInfo).exceptionally(err->{
            log.error("save async error:",err);
            return null;
        });
        return 0;
    }

    @GetMapping("/device_info/count")
    public CompletionStage<Integer> getCount(){
        return deviceInfoService.countDeviceInfo();
    }

    @GetMapping("/device_info/list/sync")
    public List<DeviceInfoEntity> selectPage(Integer page,Integer size){
        return deviceInfoService.selectPage(page,size);
    }

    @GetMapping("/device_info/page/async")
    public CompletionStage<OffsetPager.Page<DeviceInfoEntity>> selectPageAsync(Integer page,Integer size){
        return deviceInfoService.selectPageAsync(page,size);
    }
}
