package com.example.cassandra.dao;

import com.datastax.oss.driver.api.core.MappedAsyncPagingIterable;
import com.datastax.oss.driver.api.core.PagingIterable;
import com.datastax.oss.driver.api.core.cql.BoundStatement;
import com.datastax.oss.driver.api.mapper.annotations.Dao;
import com.datastax.oss.driver.api.mapper.annotations.Insert;
import com.datastax.oss.driver.api.mapper.annotations.Query;
import com.datastax.oss.driver.api.mapper.entity.saving.NullSavingStrategy;
import com.example.cassandra.entity.DeviceInfoEntity;

import java.util.concurrent.CompletionStage;

@Dao
public interface DeviceInfoDao {

    @Insert(nullSavingStrategy = NullSavingStrategy.DO_NOT_SET)
    CompletionStage<Void> saveOne(DeviceInfoEntity deviceInfoEntity);

    @Insert
    BoundStatement saveOneBound(DeviceInfoEntity deviceInfoEntity);

    @Insert
    DeviceInfoEntity saveOneDevice(DeviceInfoEntity deviceInfo);

    @Query("select * from mydb.device_info where product_id=:productId and device_id=:deviceId")
    PagingIterable<DeviceInfoEntity> selectByProductIdAndDeviceId(Long productId,String deviceId);

    @Query("select * from mydb.device_info where product_id=:productId and device_id=:deviceId")
    CompletionStage<MappedAsyncPagingIterable<DeviceInfoEntity>> selectByProductIdAndDeviceIdAsync(Long productId, String deviceId);
}
