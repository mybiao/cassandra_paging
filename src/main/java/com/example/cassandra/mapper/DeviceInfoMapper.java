package com.example.cassandra.mapper;

import com.datastax.oss.driver.api.mapper.annotations.DaoFactory;
import com.datastax.oss.driver.api.mapper.annotations.DaoKeyspace;
import com.datastax.oss.driver.api.mapper.annotations.DaoTable;
import com.datastax.oss.driver.api.mapper.annotations.Mapper;
import com.example.cassandra.dao.DeviceInfoDao;

@Mapper
public interface DeviceInfoMapper {

    @DaoFactory
    DeviceInfoDao deviceInfoDao(@DaoKeyspace String keyspace,
                                @DaoTable String table);
}
